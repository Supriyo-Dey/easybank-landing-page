const checkbox = document.getElementById('custom-checkbox');
const targetDiv = document.querySelector('.checkbox-active');

checkbox.addEventListener('change', function() {
  if (checkbox.checked) {
    targetDiv.style.display = 'flex';
    document.querySelector(".close").style.display = "block";
    document.querySelector(".hamburger").style.display = "none";
  } else {
    targetDiv.style.display = 'none';
    document.querySelector(".close").style.display = "none";
    document.querySelector(".hamburger").style.display = "block";
  }
});